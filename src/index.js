import React from "react";
import ReactDom from "react-dom";
import Heading from "./Heading";
import List from "./List.jsx";

const date = new Date();
const today = date.getHours();
var message = null;
console.log("Value of current time - " + today);
if (today < 12 && today > 0) {
  message = "Good Morning";
} else if (today > 12 && today < 18) {
  message = "Good Afternoon";
}

//In-line css style
const style = {
  color: "blue",
  fontSize: "20px"
};
ReactDom.render(
  <div>
    <Heading />
    <h1 className="heading" style={style}>
      {message}{" "}
    </h1>
    <List />
  </div>,

  document.getElementById("root")
);
