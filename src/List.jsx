import React from "react";

function List() {
  return (
    <ul>
      <li>Bacon</li>
      <li>Ham</li>
      <li>Bubble</li>
    </ul>
  );
}

export default List;
